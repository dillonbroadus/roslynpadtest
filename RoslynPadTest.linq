<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\WPF\PresentationCore.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\WPF\PresentationFramework.dll</Reference>
  <NuGetReference>RoslynPad.Editor.Windows</NuGetReference>
  <NuGetReference>RoslynPad.Roslyn</NuGetReference>
  <NuGetReference>RoslynPad.Roslyn.Windows</NuGetReference>
  <Namespace>ICSharpCode.AvalonEdit.Highlighting</Namespace>
  <Namespace>RoslynPad.Editor</Namespace>
  <Namespace>RoslynPad.Roslyn</Namespace>
  <Namespace>RoslynPad.Roslyn.Diagnostics</Namespace>
  <Namespace>RoslynPad.Roslyn.QuickInfo</Namespace>
  <Namespace>System.Collections.Immutable</Namespace>
  <Namespace>System.ComponentModel</Namespace>
  <Namespace>System.Windows.Controls</Namespace>
  <Namespace>Microsoft.CodeAnalysis</Namespace>
  <Namespace>Microsoft.CodeAnalysis.CSharp</Namespace>
  <Namespace>System.Windows</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{	
	var assemblyPath = @"C:\Users\dillonbroadus\Test.dll";
	var roslynHost = new RoslynHost(additionalAssemblies: new[]
	{
		Assembly.Load("RoslynPad.Roslyn.Windows"),
		Assembly.Load("RoslynPad.Editor.Windows")
	}, RoslynHostReferences.NamespaceDefault.With(assemblyReferences: defaultAssemblies));

	var editor = new RoslynCodeEditor();
	var workingDirectory = Directory.GetCurrentDirectory();

	var documentId = editor.Initialize(
		roslynHost: roslynHost,
		highlightColors: new ClassificationHighlightColors(),
		workingDirectory: workingDirectory,
		documentText: "void Main() { Console.WriteLine(\"Hello World!\"); }");

	editor.IsBraceCompletionEnabled = true;
	editor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("C#");

	var compileBtn = new Button { Content = "Run" };
	compileBtn.Click += (o, e) =>
	{
		Task.Run(async () =>
		{
			var syntaxTree = await roslynHost.GetDocument(documentId).GetSyntaxTreeAsync();
			var compilation = CSharpCompilation.Create("Test.dll", new SyntaxTree[] { syntaxTree }, defaultAssemblies.Select(x => MetadataReference.CreateFromFile(x.Location)),
				new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary)
					.WithUsings(new[] { "System", "System.Linq", "LINQPad" }));
			compilation.Emit(assemblyPath);
			var assembly = Assembly.Load(File.ReadAllBytes(assemblyPath));
			Activator.CreateInstance(assembly.GetType("Script")).InstanceMethodCall("Main");
		});
	};

	PanelManager.StackWpfElement(editor);
	PanelManager.StackWpfElement(compileBtn);
}

public static class Extensions
{
	public static object InstanceMethodCall(this object obj, string name, object[] args = null)
	{
		return obj.GetType().GetMethod(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Invoke(obj, args);
	}
}

private static readonly ImmutableArray<Type> defaultTypes = ImmutableArray.CreateRange(new[]
{
		typeof(object),                                // System.Runtime
        typeof(Console),                               // System.Console
        typeof(Enumerable),                            // System.Linq
        typeof(ImmutableArray),                        // System.Collections.Immutable
        typeof(INotifyPropertyChanged),                // System.ObjectModel
		typeof(LINQPad.Util)
});

private static readonly ImmutableArray<Assembly> defaultAssemblies = ImmutableArray.CreateRange(defaultTypes.Select(t => t.Assembly));

// Define other methods and classes here
